<?
//$txt[en][Error]='Error';
$txt[ru][Error]='������';
$txt[ru][On]='���';
$txt[ru][Off]='����';
$txt[ru][allow_url_fopen]='��������� ������ � ���������� ���������, �������������� �� ���������� http � ftp, ��� � �������';
$txt[ru][file_uploads]='��������� ���������� �����';
$txt[ru][include_path]='������ ���������� (����������� ����������) � ������� ������� require, include, fopen(), file(), readfile() � file_get_contents() ���� �����';
$txt[ru][open_basedir]='��������� ������ ������ � ��� ������ � ��������, ������� ��������� � ��������� ����������� (����������� - ���������)';
$txt[ru][upload_tmp_dir]='��������� ����������, ������������ ��� �������� ������ �� ����� �����������. ������ ���� �������� ��� ������.';
$txt[ru][error_log]='��� �����, � ������� ����� ����������� ��������� �� ������� PHP-��������';
$txt[ru][max_input_time]='������������ �����, � ������� �������� ������ ������ ��������� ��� ������� ������. ������������ ���������� ����� ������� ������.';
$txt[ru][max_execution_time]='������������ ����� ���������� PHP-������� (� ��������). ��� ���������� ������� ����������� ������ ����� ������������� ��������.';
//$txt[en][max_execution_time]='The maximum time in seconds a script is allowed to run before it is terminated.';
$txt[ru][memory_limit]='����������� �� ������������� ����������� ������ PHP-���������';
//$txt[en][memory_limit]='The maximum amount of memory in bytes a script is allowed to allocate. Set the value to -1 to have no memory limit (not recommended). Use shortcuts for byte values: K (kilo), M (mega), and G (giga). For example, 128M';
$txt[ru][post_max_size]='������������ ������ POST-�������';
//$txt[en][post_max_size]='The maximum size in bytes of data that can be posted with the POST method. Typically, should be larger than upload_max_filesize and smaller than memory_limit. Use shortcuts for byte values: K (kilo), M (mega), and G (giga). For example, 16M.';
$txt[ru][upload_max_filesize]='������������ ������ ������������ ����� PHP-������� ������';
//$txt[en][upload_max_filesize]='The maximum size in bytes of an uploaded file. Use shortcuts for byte values: K (kilo), M (mega), and G (giga). For example, 128M.';
$txt[ru][register_globals]='�������������� ���������� ����������';
//$txt[en][register_globals]='Tells whether to register the contents of the EGPCS (Environment, GET, POST, Cookie, Server) variables as global variables. When on, register_globals will inject your scripts with all sorts of variables, like request variables from HTML forms. This option is a great security risk, thus do not turn it on without necessity';
$txt[ru][display_errors]='�������� ��������� �� ������� ��� ����� ���������� ������ PHP-�������. �� ����������� ������������ ����������� ��� ��������� ������ ����� ����� ���������� ����� �������.';
//$txt[en][display_errors]='Determines whether errors should be printed to the screen as part of the output or if they should not be shown to a user';
$txt[ru][log_errors]='��������� ��������� �� ������� ������ PHP-������� � ���-����. �� ����������� ������������ ����������� ��� ������������ ������ ����� ������ display_errors ����� ���������� ����� �������.';
//$txt[en][log_errors]="Tells whether to log errors. By default, errors are logged in the server's error log. Use the error_log directive to specify the path to your own log file.";
$txt[ru]['date.timezone']='��������� ����, ������������ �� ��������� ����� ��������� ����/�������';
$txt[ru][error_reporting]='������� ���������������� ������';
#[sendmail_from]='����� ����������� �����, ������� ����� �������������� ��� �������� ����� php ���������';
?>
