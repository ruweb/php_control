<?php
//ini_set('error_reporting',E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING);
$cfg=Array();
$cfg['version']=phpversion();
$cfg['sapi']=php_sapi_name();
$cfg['scanned']=php_ini_scanned_files();
$cfg['inifile']=php_ini_loaded_file();
if (PHP_VERSION_ID<50300) {
    function arr_alter(&$item1, $key){
        $item1 = $item1['global_value'];
    }
    $cfg['ini']=ini_get_all();
    array_walk($cfg['ini'], 'arr_alter');
} else $cfg['ini']=ini_get_all(null,0);
$cfg['ext']=get_loaded_extensions();
$cfg['zend']=get_loaded_extensions(true);
echo serialize($cfg);
