<?
$lang='ru';
#$lang='en';

$_GET = Array();
$QUERY_STRING=getenv('QUERY_STRING');
if ($QUERY_STRING != "")
{
            parse_str(html_entity_decode($QUERY_STRING), $get_array);
            foreach ($get_array as $key => $value)
            {
                        $_GET[urldecode($key)] = urldecode($value);
            }
}

$_POST = Array();
$POST_STRING=getenv('POST');
if ($POST_STRING != "")
{
            $post_array=parseQueryString(html_entity_decode($POST_STRING));
            foreach ($post_array as $key => $value)
            {
                        $_POST[urldecode($key)] = urldecode($value);
            }
}

$params=$_GET+$_POST;

$txt=Array();
if (!$_ENV[LANGUAGE] && $_GET[lang]) $_ENV[LANGUAGE]=$_GET[lang];
if (file_exists("$_ENV[DOCUMENT_ROOT]/lang.php")) include_once("$_ENV[DOCUMENT_ROOT]/lang.php");
if ($_ENV[LANGUAGE] && $txt["$_ENV[LANGUAGE]"]) $lang=$_ENV[LANGUAGE];

parse_str(str_replace('; ','&',$_SERVER[HTTP_COOKIE]),$_COOKIE);

if (!$params[skin] && $_COOKIE[skin]) $params[skin]=$_COOKIE[skin];
if (!$params[charset] && $_COOKIE[charset]) $params[charset]=$_COOKIE[charset];
if ($lang=='ru' && $CHARSET!='utf-8' && $params[skin]!='power_ru' && $params[charset]!='windows-1251' && (extension_loaded('iconv') || @dl('iconv.so'))) {
    iconv_set_encoding("internal_encoding", "WINDOWS-1251");
    iconv_set_encoding("output_encoding", "UTF-8");
    ob_start("ob_iconv_handler");
}
//### !!! DO NOT START OUTPUT BEFORE ob_start() !!!

if ($_COOKIE[skin]!=$params[skin]) echo '<script>document.cookie="skin='.rawurlencode($params[skin]).';expires='.(60*60*24*365)."\";</script>\n";
if ($_COOKIE[charset]!=$params[charset]) echo '<script>document.cookie="charset='.rawurlencode($params[charset]).';expires='.(60*60*24*365)."\";</script>\n";

#if (!$params[domain]) {message("Error occured", "Domain name not specified"); exit;}
if (!$params[domain]) $params[domain]=$_ENV[SESSION_SELECTED_DOMAIN];

if (!extension_loaded('pcre')) dl('pcre.so');

$USER=$_SERVER[USER];

function message($text,$details=''){
	echo '
<p><table width=50% height=100 cellspacing=0 cellpadding=5>
    <tr>
      <td height="50%" valign="middle" align="center">
        <p align="center">'.$text.'</p>
      </td>
    </tr>
    <tr>
    	<td height=1 valign="middle" align="center">
    		<table width = 50%>
    			<tr><td bgcolor="#C0C0C0"> </td></tr>
    		</table>
    	</td>
    </tr>
    <tr>
      <td height="50%" valign="middle" align="center">';
	if ($details=='back') echo '<p><a href="javascript:history.back()">Back</a></p>';
	elseif ($details) echo '
        <p align="center"><b>Details</b></p>
        <p align="center">'.$details.'</p>';
	echo '
      </td>
    </tr>
</table>
</p>
';
	exit;
}


function mkdir_r($path,$mode=FALSE){
    $path=rtrim($path,'/');
    if (file_exists($path) && is_dir($path)) return TRUE;
    else {
	$dir = substr($path,0,strrpos($path, '/'));
	if (mkdir_r($dir,$mode)) return ($mode==FALSE?mkdir($path):mkdir($path,$mode));
	else return FALSE;
    }
}

function delTree($dir){
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
	(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

function options($options,$selected='',$firstempty=' !@#',$skip=' !@#'){
	if ($firstempty!=' !@#' && ($firtempty || $firstempty==$selected)) $result='<option></option>';
	while (list($key,$val)=each($options)) if ($skip!="$key")
		$result.="\n<option value='$key'".($selected=="$key"?' selected':'').">$val</option>";
	return $result;
}

function passwd($len=8){
    $pass='';
    $arr=split(",","A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9");
    for ($i=1;$i<=$len;$i++) {
	$pass.=$arr[rand(0,count($arr)-1)];
    }
    return $pass;
}

function parseQueryString($str) {
    $op = array();
    $pairs = explode("&", $str);
    foreach ($pairs as $pair) {
        list($k, $v) = array_map("urldecode", explode("=", $pair)); 
        $op[$k] = $v;
    }
    return $op;
}

$PAGE_FOOTER="\n</p>\n<br>\n$PLUGIN_NAME � $PLUGIN_YEAR <a href=http://ruweb.net>RuWeb.net</a></center>";
echo "<center><br><a href='?domain=$params[domain]'><b>".($PLUGIN_TITLE?$PLUGIN_TITLE:$PLUGIN_NAME)."</b></a><br>\n<p>\n";
?>