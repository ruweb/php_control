#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

[ -z "$DOCUMENT_ROOT" ] && DOCUMENT_ROOT="./"

cd "$DOCUMENT_ROOT"

TIME=`date +"%y%m%d%H%M%S"`
CUSTDIR="/usr/local/directadmin/scripts/custom"

for conf in /usr/local/directadmin/data/users/*/user.conf; do
    if [ 0`grep -c skin=power_ru $conf` -gt 0 ]; then
	perl -pi -e 's#language=en#language=ru#' $conf
    fi
done

for file in update_post.sh update_post.skins.sh; do
    [ ! -e "$CUSTDIR/$file" ] \
        && cp -pv "files/$file" "$CUSTDIR/" \
        && chown diradmin:diradmin "$CUSTDIR/$file"
done


chflags -h noschg /home/*/php && chflags -h uchg /home/*/php


for file in `grep -lR "chflags .*schg " "$CUSTDIR/"`; do
    perl -pi -e 's/(chflags\s.*?)schg\s/$1uchg /' "$file"
    echo "$file" converted
done

[ ! -e "$CUSTDIR/user-php.fcgi-wrapper" ] && cp -pv user-php.fcgi-wrapper "$CUSTDIR/user-php.fcgi-wrapper"
grep -q 'echo "#!/usr/local/bin/php-cgi" > /home/$username/php' "$CUSTDIR/user_create_post.sh" && \
    perl -pi -e 's|echo "#!/usr/local/bin/php-cgi" > /home/\$username/php|cp /usr/local/directadmin/scripts/custom/user-php.fcgi-wrapper /home/\$username/php|' "$CUSTDIR/user_create_post.sh" \
    && echo "user_create_post.sh modified for new wrapper template"

([ -e "$CUSTDIR/user-php.ini" ] && diff -q user-php.ini "$CUSTDIR/user-php.ini") || \
    ([ -e "$CUSTDIR/user-php.ini" ] && cp -p "$CUSTDIR/user-php.ini" "user-php.ini~$TIME"; cp -pv user-php.ini "$CUSTDIR/user-php.ini")
grep -q 'mbstring.func_overload = 4" > /home/$username/php.ini' "$CUSTDIR/user_create_post.sh" && \
    perl -i -ne 'print "[ -e /home/\$username/php.ini ] || cat /usr/local/directadmin/scripts/custom/user-php.ini > /home/\$username/php.ini\n"
        if m|echo "session.save_path = /home/.username/tmp|;
    $f=1 if m|upload_tmp_dir = /home/.username/tmp|;
    $f++ if $f>0;
    $f=-1 if s|.*(> /home/.username/php.ini)|" >$1|;
    print if $f<=2' "$CUSTDIR/user_create_post.sh" \
    && echo "user_create_post.sh modified for new ini template"

#EXTENSIONS=`diff -ubB /usr/local/etc/php-fcgi.ini php-fcgi.ini | egrep '^- *(zend_|)extension' | tr -d '-'`
EXTENSIONS=`cat /usr/local/etc/php/*.ini | grep -Fxvf php-fcgi.ini | egrep '^ *(zend_|)extension *='`
[ -e /usr/local/etc/php-fcgi.ini ] && \
    EXTENSIONS2=`grep -Fxvf php-fcgi.ini /usr/local/etc/php-fcgi.ini | egrep '^ *(zend_|)extension *='`
for user in `ls /usr/local/directadmin/data/users/`; do
    ([ ! -f "/home/$user/php" ] || [ -L "/home/$user/php" ]) && continue
    if [ -e "/home/$user/php.ini" ]; then
        [ -L "/home/$user/php.ini" ] && continue
    else
         rm -f "/home/$user/php.ini" && touch "/home/$user/php.ini" && chown -h "$user:$user" "/home/$user/php.ini"
    fi
    if grep -q '/usr/local/etc/php-fcgi.ini' "/home/$user/php"; then
        if [ -e /usr/local/etc/php-fcgi.ini ]; then
            if grep -q '^ *mbstring.func_overload *= *4' /usr/local/etc/php-fcgi.ini && ! grep -q '^ *mbstring.func_overload' "/home/$user/php.ini"; then
                INSERT="mbstring.func_overload = 4
";
            else
                INSERT=""
            fi
            [ -n "$INSERT$EXTENSIONS2" ] && printf "\n$INSERT$EXTENSIONS2\n" >> "/home/$user/php.ini" && echo "/home/$user/php.ini updated"
        else
            [ -n "$EXTENSIONS" ] && printf "\n$EXTENSIONS\n" >> "/home/$user/php.ini" && echo "/home/$user/php.ini updated"
        fi
    else
        grep -q '#!/usr/local/bin/php-cgi' "/home/$user/php" && chflags nouchg "/home/$user/php" && \
            cp -v "$CUSTDIR/user-php.fcgi-wrapper" "/home/$user/php" && chflags uchg "/home/$user/php"
        [ -n "$EXTENSIONS" ] && printf "\n$EXTENSIONS\n" >> "/home/$user/php.ini" && echo "/home/$user/php.ini updated"
    fi
done

if [ -e /usr/local/etc/php-fcgi.ini ]; then
    diff -q php-fcgi.ini /usr/local/etc/php-fcgi.ini || (\
        cp -p /usr/local/etc/php-fcgi.ini "php-fcgi.ini~$TIME"
        cp -pv php-fcgi.ini /usr/local/etc/php-fcgi.ini
    )
else
    cp -pv php-fcgi.ini /usr/local/etc/php-fcgi.ini
fi

echo "Plugin Installed!"; #NOT! :)

#. "$DOCUMENT_ROOT/update.sh"

#chmod 755 $DOCUMENT_ROOT/../admin

exit 0;
