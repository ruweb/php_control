#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

for ini in /usr/local/etc/php-fcgi*.ini; do
    extensions=$(perl -ne 'if (m|^\s*((zend_)?extension)\s*=\s*(.*/)?([^\.]+.so)|) { print "$4\n"}' "$ini")
    for php in $(grep -l "$ini" /home/*/php); do
        [ -e "$php.ini" ] || continue
        updated=0
        for ext in $extensions; do
            grep -E '^[[:space:]]*(zend_|)extension' "$php.ini" | grep -q "$ext" \
                && perl -pi -e "s|^(\s*(zend_)?extension\s*=\s*(.*/)?$ext)|;\$1|" "$php.ini" && updated=1
        done
        [ "0$updated" -gt 0 ] && echo "$php.ini updated"
    done
done

echo "Plugin has been updated!"; #NOT! :)

exit 0;
