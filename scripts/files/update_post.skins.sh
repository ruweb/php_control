#!/bin/sh

cd /usr/local/directadmin/data/skins/

SKINS="https://bitbucket.org/ruweb/power_ru/get/master.tar.gz;power_ru
https://github.com/tuthost/directadmin-enhanced-ru/tarball/master;.directadmin-enhanced-ru"

echo update skins...

for str in $SKINS; do
    url=${str%%;*}
    if [ "$url" != "$str" ]; then
        skin=${str##*;}
        file="$skin.tar.gz"
	str="--strip-components=1"
    else
	file=${str##*/}
	skin=${file%%.tar.gz}
	str=" "
    fi
    echo
    echo $skin...
    mod=" "
    STAT=`stat "$file" 2>/dev/null` && mod="-i $file"
    fetch --no-verify-peer -m $mod -o $file "$url" || continue
    if [ "$STAT" != "`stat $file 2>/dev/null`" -o ! -e "$skin/" ]; then
        gunzip -t $file || continue
	rm -rf "./$skin/"
	mkdir -p "$skin" && tar xz $str -C ./$skin/ -f ./$file
    fi
done
echo

echo update ru/ua lang for enhanced skin...
rm -f enhanced/files_custom.conf
rm -rf enhanced/lang/ru enhanced/lang/ua \
        && cp -R .directadmin-enhanced-ru/skins/enhanced/lang/ru .directadmin-enhanced-ru/skins/enhanced/lang/ua enhanced/lang/ \
        && cp .directadmin-enhanced-ru/skins/enhanced/images/[ru][ua]-* enhanced/images/ \
        && echo "LANG_WEB_CP=Web Control Panel" > enhanced/lang/ru/lf_footer.html \
        && echo "LANG_WEB_CP=Web Control Panel" > enhanced/lang/ua/lf_footer.html \
        && cat .directadmin-enhanced-ru/skins/enhanced/files_user.conf | tr -d '\r' | grep -F -x -v -f enhanced/files_user.conf >> enhanced/files_user.conf \
        && cat .directadmin-enhanced-ru/skins/enhanced/files_reseller.conf | tr -d '\r' | grep -F -x -v -f enhanced/files_reseller.conf >> enhanced/files_reseller.conf \
        && cat .directadmin-enhanced-ru/skins/enhanced/files_admin.conf | tr -d '\r' | grep -F -x -v -f enhanced/files_admin.conf >> enhanced/files_admin.conf

for file in `ls enhanced/lang/ru/internal/`; do
    egrep -o '(^[0-9]+=)' power_ru/lang/ru/internal/$file | sed -e 's/^/^/' | grep -vf - enhanced/lang/ru/internal/$file \
        | iconv -f utf-8 -t cp1251 >> power_ru/lang/ru/internal/$file
done

#echo fix Capri...
#perl -pi -e 's/^\s+if \(\$SSL == 1\)/if (\$SSL == 1 && \$_SERVER_PORT!=2223)/' Capri/inc/functions.php Capri/inc/func.php

#echo fix ru lang for skins without ru...
#for dir in $(ls -1d */); do
#    dir=${dir%%/}
#    if [ -d "$dir/lang" -a -e "$dir/lang/en" -a ! -e "$dir/lang/ru" ]; then
#	ln -s en "$dir/lang/ru"
#    fi
#done

echo switch english skins to utf8...
perl -pi -e 's/^LANG_ENCODING=iso-8859-1$/LANG_ENCODING=UTF-8/' */lang/en/lf_standard.html
perl -pi -e 's/; charset=windows-1252">/; charset=utf-8">/' power_user/header.html
perl -pi -e 's#^<HEAD>$#<HEAD><meta http-equiv="Content-Type" content="text/html; charset=utf-8">#' default/header.html

chown -Rh diradmin:diradmin /usr/local/directadmin/data/skins/*/

if [ -e /usr/local/directadmin/data/users/rubill ]; then
    perl -pi -e 's#^(skin=|docsroot=./data/skins/)power_ru.*#\1default#' /usr/local/directadmin/data/users/rubill/user.conf
    perl -pi -e 's#^(language=)ru.*#\1en#' /usr/local/directadmin/data/users/rubill/user.conf
fi

echo "=== power_ru skin updated! ==="
echo
